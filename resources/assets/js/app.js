
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('axios');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('report-component', require('./components/reports/Reports.vue'));

// Journal entries components
Vue.component('journal-entries-list-component', require('./components/journal_entries/JournalEntriesList.vue'));
Vue.component('journal-entries-record-component', require('./components/journal_entries/JournalEntriesRecord.vue'));

Vue.component('account-chart-component', require('./components/settings/AccountCharts.vue'));

Vue.component('menu-component', require('./components/headers/Header.vue'));
Vue.component('home', require('./components/home/Home.vue'));
Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});
