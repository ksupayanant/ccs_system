<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'Home\HomeController');

Route::resource('journal_entries/list', 'Accounts\JournalEntries\JournalEntriesListController');

Route::resource('journal_entries/record', 'Accounts\JournalEntries\JournalEntriesRecordController');

Route::resource('setting/accountchart', 'Setting\AccountChartController');




Route::resource('accountcode', 'Accounts\AccountCodeController');

Route::resource('accountcategory', 'Accounts\AccountCategoryController');
Route::get('accountcategory/{id}/accountcodes', 'Accounts\AccountCategoryController@showAllCodeInCategory');

Route::resource('reports', 'Accounts\ReportsController');

