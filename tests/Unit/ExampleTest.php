<?php

namespace Tests\Unit;

use App\Model\GeneralRecords;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateGeneralRecord()
    {
        $generalRecord = factory(GeneralRecords::class)->create(["date" => "2017-10-08","documentType" => "เอกสาร"]);
        print($generalRecord);
        $this->assertTrue(true);
    }
}
