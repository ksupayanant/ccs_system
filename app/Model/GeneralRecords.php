<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralRecords extends Model
{
    protected $table = 'general_records';
}
