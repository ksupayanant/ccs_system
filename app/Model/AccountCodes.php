<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccountCodes extends Model
{
    protected $table = 'account_codes';
    protected $fillable = ['account_code','account_name'];
}
