<?php

namespace App\Http\Controllers\Accounts;

use App\Model\AccountCategory;
use App\Model\AccountCodes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountCategory = AccountCategory::all();
        return $accountCategory;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $accountCodes
     * @return \Illuminate\Http\Response
     */
    public function show($accountCategory)
    {
        try {
            $accountCategoryRecord = AccountCategory::where([
                ['account_category_id', '=', $accountCategory],
                ['rec_status', '=', '1']
            ])->firstOrFail();
            return $accountCategoryRecord;
        } catch (ModelNotFoundException $e) {
            $response = new Response();
            return $response->setStatusCode(400, 'Your account code : ' . $accountCategory . 'cannot be found!');
        }
    }

    public function showAllCodeInCategory($id){
        try {
            $accountCategoryRecord = AccountCodes::join('account_category','account_codes.account_category_id','account_category.account_category_id')
                ->where([
                    ['account_category.account_category_id',$id],
                    ['account_category.rec_status',1],
                    ['account_codes.rec_status',1]
                ])
                ->get();
            return $accountCategoryRecord;
        } catch (ModelNotFoundException $e) {
            $response = new Response();
            return $response->setStatusCode(400, 'Your account code : ' . $id . 'cannot be found!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\AccountCategory  $accountCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountCategory $accountCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\AccountCategory  $accountCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountCategory $accountCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\AccountCategory  $accountCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountCategory $accountCategory)
    {
        //
    }
}
