<?php

namespace App\Http\Controllers\Accounts;

use App\Model\AccountCodes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AccountCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AccountCodes::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $accountCodes
     * @return \Illuminate\Http\Response
     */
    public function show($accountCodes)
    {
            try {
                $accountCode = AccountCodes::where([
                    ['account_code', '=', $accountCodes],
                    ['rec_status', '=', '1']
                ])->firstOrFail();
                return $accountCode;
            } catch (ModelNotFoundException $e) {
                $response = new Response();
                return $response->setStatusCode(400, 'Your account code : ' . $accountCodes . 'cannot be found!');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\AccountCodes  $accountCodes
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountCodes $accountCodes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\AccountCodes  $accountCodes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountCodes $accountCodes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\AccountCodes  $accountCodes
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountCodes $accountCodes)
    {
        //
    }
}
