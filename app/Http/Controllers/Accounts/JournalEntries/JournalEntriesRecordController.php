<?php

namespace App\Http\Controllers\Accounts\JournalEntries;

use App\Model\GeneralRecords;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JournalEntriesRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('journal_entries/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $generalRecords = $request->input('generalRecords');
            foreach ($generalRecords as $generalRecord) {
                $req = new GeneralRecords();
                $req->date = $generalRecord["date"];
                $req->type_of_document_id = $generalRecord["type_of_document_id"];
                $req->record_no = $generalRecord["record_no"];
                $req->category_id = $generalRecord["category_id"];
                $req->account_id = $generalRecord["account_id"];
                $req->credit_amount = $generalRecord["credit_amount"];
                $req->debit_amount = $generalRecord["debit_amount"];
                $req->save();
            }
            return response($generalRecords, 200)
                ->header('Content-Type', 'application/json');
        }catch(\Exception $e){
            return response($e, 200)
                ->header('Content-Type', 'application/json');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\GeneralRecords  $generalRecords
     * @return \Illuminate\Http\Response
     */
    public function show(GeneralRecords $generalRecords)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\GeneralRecords  $generalRecords
     * @return \Illuminate\Http\Response
     */
    public function edit(GeneralRecords $generalRecords)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\GeneralRecords  $generalRecords
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GeneralRecords $generalRecords)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\GeneralRecords  $generalRecords
     * @return \Illuminate\Http\Response
     */
    public function destroy(GeneralRecords $generalRecords)
    {
        //
    }
}
