<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    /**
     * @SWG\Swagger(
     *   basePath="/",
     *   @SWG\Info(
     *     title="CCS system api url",
     *     version="1.0.0"
     *   )
     * )
     */

    /**
     * @SWG\Get(
     *   path="/accountcode/{accountCode}",
     *   summary="Show account code detail",
     *   operationId="getAccountCode",
     *   tags={"account"},
     *   @SWG\Parameter(
     *     name="accountCode",
     *     in="path",
     *     description="Account code",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */

    /**
     * @SWG\Get(
     *   path="/accountcode/{accountCategory}/accountcodes",
     *   summary="Show account code detail",
     *   operationId="getAccountCodeByCategory",
     *   tags={"account"},
     *   @SWG\Parameter(
     *     name="accountCategory",
     *     in="path",
     *     description="Account code",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */

    /**
     * @SWG\Post(
     *   path="/generalrecord",
     *   summary="Show account code detail",
     *   operationId="getAccountCode",
     *   tags={"general record"},
     *   @SWG\Parameter(
     *     name="date",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="account_id",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="category_id",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="type_of_document_id",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="record_no",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="debit_amount",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="number"
     *   ),
     *   @SWG\Parameter(
     *     name="credit_amount",
     *     in="formData",
     *     description="Account code",
     *     required=true,
     *     type="number"
     *   ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
}
